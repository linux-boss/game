package starwars;

public class Explosion extends Sprite {
    final int TIME_LIFE = 5;
    private int timeLife;

    public Explosion(int x, int y) {
        super(x, y);
        initExplosion();
    }

    private void initExplosion() {
        loadImage("/explosion.png");
        getImageDimensions();
        timeLife = TIME_LIFE;
    }

    public void modifyTimeLife() {
        if (--timeLife == 0) setVisible(false);
    }
}
