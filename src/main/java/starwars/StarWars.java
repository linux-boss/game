package starwars;

import javax.swing.*;
import java.awt.*;

public class StarWars extends JFrame {

    public StarWars() {
        initUI();
    }

    private void initUI() {
        add(new Board());
        setResizable(false);
        pack();
        setTitle("Star wars");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            StarWars ex = new StarWars();
            ex.setVisible(true);
        });
    }
}