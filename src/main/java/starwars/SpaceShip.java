package starwars;

import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SpaceShip extends Sprite {

    private int dx;
    private int dy;
    private List<Missile> missiles;
    private Sound shotSound;

    public SpaceShip(int x, int y) {
        super(x, y);
        initCraft();
        initSound();
    }

    private void initSound() {
        InputStream shotStream = getClass().getResourceAsStream("/shot.wav");
        shotSound = new Sound(shotStream);
    }

    private void initCraft() {
        missiles = new ArrayList<>();
        loadImage("/spaceship.png");
        getImageDimensions();
    }

    public void move() {
        x += dx;
        y += dy;
        if (x < 1) {
            x = 1;
        }
        if (y < 1) {
            y = 1;
        }
    }

    public List<Missile> getMissiles() {
        return missiles;
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            dx = -1;
        }
        if (key == KeyEvent.VK_RIGHT) {
            dx = 1;
        }
        if (key == KeyEvent.VK_UP) {
            dy = -1;
        }
        if (key == KeyEvent.VK_DOWN) {
            dy = 1;
        }
    }

    public void fire() {
        missiles.add(new Missile(x + width, y + height / 2));
        shotSound.play(true);
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }
        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }
        if (key == KeyEvent.VK_UP) {
            dy = 0;
        }
        if (key == KeyEvent.VK_DOWN) {
            dy = 0;
        }
    }

    public void keyTyped(KeyEvent e) {
        int key = e.getKeyChar();
        if (key == KeyEvent.VK_SPACE) {
            fire();
        }
    }
}