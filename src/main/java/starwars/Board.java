package starwars;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Board extends JPanel implements ActionListener {
    private Timer timer;
    private SpaceShip spaceship;
    private List<Alien> aliens;
    private List<Explosion> explosions;
    private boolean ingame;
    private int win;
    private final int B_WIDTH = 400;
    private final int B_HEIGHT = 300;

    private final int[][] pos = {
            {2380, 29}, {2500, 59}, {1380, 89},
            {780, 109}, {580, 139}, {680, 239},
            {790, 259}, {760, 50}, {790, 150},
            {980, 209}, {560, 45}, {510, 70},
            {930, 159}, {590, 80}, {530, 60},
            {940, 59}, {990, 30}, {920, 200},
            {900, 259}, {660, 50}, {540, 90},
            {810, 220}, {860, 20}, {740, 180},
            {820, 128}, {490, 170}, {700, 30},
            {1200, 80}, {950, 120}, {750, 200}
    };

    private Sound music;
    private Sound explosionSound;
    private BufferedImage background;


    public Board() {
        initBoard();
    }

    private void initBoard() {
        addKeyListener(new TAdapter());
        setFocusable(true);
        try {
            InputStream backgroundImg = getClass().getResourceAsStream("/background.png");
            background = ImageIO.read(Objects.requireNonNull(backgroundImg));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ingame = true;

        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));

        int ICRAFT_X = 40;
        int ICRAFT_Y = 60;
        spaceship = new SpaceShip(ICRAFT_X, ICRAFT_Y);

        initAliens();
        initExplosions();

        int DELAY = 15;
        timer = new Timer(DELAY, this);
        timer.start();

        initSound();
    }

    public void initSound() {
        InputStream musicStream = getClass().getResourceAsStream("/music.wav");

        if (music != null) music.close();

        music = new Sound(musicStream);
        music.play();

        InputStream explosionStream = getClass().getResourceAsStream("/crash.wav");
        explosionSound = new Sound(explosionStream);
    }

    public void initAliens() {
        aliens = new ArrayList<>();
        for (int[] p : pos) {
            aliens.add(new Alien(p[0], p[1]));
        }
    }

    public void initExplosions() {
        explosions = new ArrayList<>();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, this);

        if (ingame) {
            drawObjects(g);
        } else {
            drawGameOver(g);
        }
        Toolkit.getDefaultToolkit().sync();
    }

    private void drawObjects(Graphics g) {
        if (spaceship.isVisible()) {
            g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(),this);
        }
        List<Missile> ms = spaceship.getMissiles();
        for (Missile missile : ms) {
            if (missile.isVisible()) {
                g.drawImage(missile.getImage(), missile.getX(), missile.getY(), this);
            }
        }

        for (Alien alien : aliens) {
            if (alien.isVisible()) {
                g.drawImage(alien.getImage(), alien.getX(), alien.getY(), this);
            }
        }

        for (Explosion explosion : explosions) {
            if (explosion.isVisible()) {
                g.drawImage(explosion.getImage(), explosion.getX(), explosion.getY(), this);
            }
        }

        g.setColor(Color.WHITE);
        g.drawString("Aliens left: " + aliens.size(), 5, 15);
    }

    private void drawGameOver(Graphics g) {
        String msgGameOver = "Game Over";
        String msgExitOrRepeat = "Press <Esc> to exit or any key to repeat";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics fm = getFontMetrics(small);
        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msgGameOver, (B_WIDTH - fm.stringWidth(msgGameOver)) / 2, B_HEIGHT / 3);
        g.drawString(msgExitOrRepeat, (B_WIDTH - fm.stringWidth(msgExitOrRepeat)) / 2,B_HEIGHT / 2);

        if (win == 3) {
            music.stop();
            InputStream winStream = getClass().getResourceAsStream("/popcorn.wav");
            music = new Sound(winStream);
            music.play();
            win = 0;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        updateShip();
        updateMissiles();
        updateAliens();
        updateExplosions();
        checkCollisions();
        repaint();
        inGame();
    }

   private void inGame() {
        if (!ingame) {
            timer.stop();
        }
    }

    private void updateShip() {
        if (spaceship.isVisible()) {
            spaceship.move();
        }
    }

    private void updateMissiles() {
        List<Missile> ms = spaceship.getMissiles();
        for (int i = 0; i < ms.size(); i++) {
            Missile m = ms.get(i);
            if (m.isVisible()) {
                m.move();
            } else {
                ms.remove(i--);
            }
        }
    }

    private void updateAliens() {
        if (aliens.isEmpty()) {
            ingame = false;
            win++;
            return;
        }
        for (int i = 0; i < aliens.size(); i++) {
            Alien a = aliens.get(i);
            if (a.isVisible()) {
                a.move();
            } else {
                aliens.remove(i--);
            }
        }
    }

    private void updateExplosions() {
        if (!explosions.isEmpty()) {
            for (int i = 0; i < explosions.size(); i++) {
                Explosion explosion = explosions.get(i);
                if (explosion.isVisible()) {
                    explosion.modifyTimeLife();
                } else {
                    explosions.remove(i--);
                }
            }
        }
    }

    public void checkCollisions() {
        Rectangle r3 = spaceship.getBounds();
        for (Alien alien : aliens) {
            Rectangle r2 = alien.getBounds();
            if (r3.intersects(r2)) {
                spaceship.setVisible(false);
                alien.setVisible(false);
                ingame = false;
                win = 0;
            }
        }
        List<Missile> ms = spaceship.getMissiles();
        // Проверяем поподание ракетой в НЛО
        for (Missile m : ms) {
            Rectangle r1 = m.getBounds();
            for (Alien alien : aliens) {
                Rectangle r2 = alien.getBounds();
                if (r1.intersects(r2)) {
                    // Попали. Гасим ракету и НЛО
                    m.setVisible(false);
                    alien.setVisible(false);
                    // Добавляем взрыв
                    explosions.add(new Explosion(alien.getX(), alien.getY()));
                    explosionSound.play(true);
                }
            }
        }
    }

    private class TAdapter extends KeyAdapter {
        @Override
        public void keyReleased(KeyEvent e) {
                spaceship.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (!ingame) {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ESCAPE) {
                    System.exit(0);
                }
                music.close();
                initBoard();
            } else {
                spaceship.keyPressed(e);
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
            spaceship.keyTyped(e);
        }
    }
}