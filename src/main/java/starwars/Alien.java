package starwars;


public class Alien extends Sprite {

    public Alien(int x, int y) {
        super(x, y);
        initAlien();
    }

    private void initAlien() {
        loadImage("/alien.png");
        getImageDimensions();
    }

    public void move() {
        if (x < 0) {
            x = 400;
        }
        x -= 1;
    }
}